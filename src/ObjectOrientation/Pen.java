package ObjectOrientation;

public class Pen {
	int inkLevel = 100;
	String color = "black";
	String message = "";
	String brand = "Elkos";

	public void openCap() {
		System.out.println("Cap is opened..");
	}

	public void write(String message) {
		this.message = message;
	}

	public void fillInk() {
		System.out.println("Please do refilling...");
	}

}
