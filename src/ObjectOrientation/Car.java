package ObjectOrientation;

public class Car {
	String brand = "";
	String color = "";
	int price = 0;

	public void start() {
		System.out.println("started");
	}

	public void stop() {
		System.out.println("Stoped...");
	}

	public void setUp(String brand, String color, int price) {
		this.color = color;
		this.brand = brand;
		this.price = price;
	}

	public void display() {
		System.out.println("THe company is " + this.brand + " color is " + this.color + " costs " + this.price);
	}

}
